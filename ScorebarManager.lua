ScorebarManager = {}

local function update()
    for i = 1, 7, 1 do
        local circle = ScorebarManager.circles[i]
        circle.shape.rotation = circle.shape.rotation + circle.rotationSpeed * 2
    end
end

function ScorebarManager:init()
    --if not (self.isInitialized == nil) and self.isInitialized then return end
    self.isInitialized = true
    self.root = display.newGroup()
    self.root.x = screen.center.x
    self.root.y = 0
    self.circles = {}
    self.score = 0
    self.targetScore = 0
    self.circlesCount = 0
    
    self.background = display.newRect(self.root, 0, 50, screen.size.w, 100)
    self.background.fill = paint.main.primary
    self.backgroundShadow = display.newRect(self.root, 0, 105, screen.size.w, 10)
    self.backgroundShadow.fill = paint.shadow.gradient -- @Task change paint class
    self.scoreText = display.newText(self.root, "SCORE: " .. self.score .. "/" .. self.targetScore, 0, 20, FONT, 24)
    
    local cell = self.background.width / 7
    local xpos = self.background.width - 38
    local ypos = self.background.y + self.background.height / 2
    local factor = 0
    local circle = nil

    for i = 1, 7, 1 do
        factor = i - 1
        self.circles[i] = display.newGroup()
        local circle = self.circles[i]
        circle.score = 0
        circle.rotationSpeed = i + 2
        circle.x = xpos - cell * factor
        circle.y = ypos
        circle.isVisible = false
        
        circle.backgroundShadow = display.newCircle(circle, 0, 5, 30)
        circle.background = display.newCircle(circle, 0, 0, 30)
        circle.outline = display.newCircle(circle, 0, 0, 24)
        circle.shape = display.newCircle(circle, 0, 0, 20)
        circle.scoreText = display.newText(circle, circle.score, 0, -40, FONT, 24)

        circle.backgroundShadow.fill = paint.shadow.gradient
        circle.background.fill = paint.main.primary
        circle.outline.fill = FadeGradient(paint.main.primaryDark)
        if (i == 7) then
            circle.shape.fill = FadeGradient(paint.rainbow.red, SCOREBAR_CIRCLE_SHAPE_ALPHA)
            circle.type = CIRCLE_TYPE_RED
        elseif (i == 6) then
            circle.shape.fill = FadeGradient(paint.rainbow.orange, SCOREBAR_CIRCLE_SHAPE_ALPHA)
            circle.type = CIRCLE_TYPE_ORANGE
        elseif (i == 5) then
            circle.shape.fill = FadeGradient(paint.rainbow.yellow, SCOREBAR_CIRCLE_SHAPE_ALPHA)
            circle.type = CIRCLE_TYPE_YELLOW
        elseif (i == 4) then
            circle.shape.fill = FadeGradient(paint.rainbow.green, SCOREBAR_CIRCLE_SHAPE_ALPHA)
            circle.type = CIRCLE_TYPE_GREEN
        elseif (i == 3) then
            circle.shape.fill = FadeGradient(paint.rainbow.azure, SCOREBAR_CIRCLE_SHAPE_ALPHA)
            circle.type = CIRCLE_TYPE_AZURE
        elseif (i == 2) then
            circle.shape.fill = FadeGradient(paint.rainbow.blue, SCOREBAR_CIRCLE_SHAPE_ALPHA)
            circle.type = CIRCLE_TYPE_BLUE
        elseif (i == 1) then
            circle.shape.fill = FadeGradient(paint.rainbow.violet, SCOREBAR_CIRCLE_SHAPE_ALPHA)
            circle.type = CIRCLE_TYPE_VIOLET
        end
    end

    Runtime:addEventListener("enterFrame", update)
end

function ScorebarManager:release()
    Runtime:removeEventListener("enterFrame", update)
    for i = 1, 7, 1 do
        display.remove(self.circles[i])
    end
    display.remove(self.root)
    circles = nil
end

function ScorebarManager:newLevel(levelId)
    local gamemode = GAMEMODE_LEVELS[levelId]
    self.targetScore = gamemode.score
    self.circlesCount = gamemode.circles

    local optimization = 0
    local c = self.circlesCount
    if (c >= 7) then
        self.circlesCount = 7
        optimization = 38
    elseif (c == 6) then
        optimization = 45
    elseif (c == 5) then
        optimization = 54
    elseif (c == 4) then
        optimization = 67
    elseif (c == 3) then
        optimization = 90
    elseif (c == 2) then
        optimization = 135
    elseif (c == 1) then
        optimization = 270
    end
    
    self.scoreText.text = "SCORE: " .. self.score .. "/" .. self.targetScore
    local cell = self.background.width / self.circlesCount
    local xpos = self.background.width
    local ypos = self.background.y + self.background.height / 2
    local factor = 0
    local circle = nil
    for i = 1, self.circlesCount, 1 do
        circle = self.circles[8 - i]
        factor = i - 1

        circle.x = xpos - cell * factor - optimization
        circle.y = ypos
        circle.isVisible = true
    end

end

function ScorebarManager:addScore(circleType, value)
    self.score = self.score + value
    player.score = self.score
    self.scoreText.text = "SCORE: " .. self.score .. "/" .. self.targetScore
    for i = 1, 7, 1 do
        if self.circles[i].type == circleType then
            local circle = self.circles[i]
            circle.score = circle.score + value
            circle.scoreText.text = circle.score

            if (value > 0) then
                circle.scoreAnimation1 = animator:animate({
                    id = circle.scoreAnimation1, start = 1, target = 1.25, duration = 300,
                    callback = function(value)
                        circle.outline.xScale = value; circle.outline.yScale = value
                        circle.shape.xScale = value; circle.shape.yScale = value
                    end,
                    finalCallback = function(value)
                        circle.scoreAnimation2 = animator:animate({
                            id = circle.scoreAnimation2, start = 1.25, target = 1, duration = 300,
                            callback = function(value)
                                circle.outline.xScale = value; circle.outline.yScale = value
                                circle.shape.xScale = value; circle.shape.yScale = value
                            end
                        })
                    end
                })
            else
                circle.scoreAnimation3 = animator:animate({
                    id = circle.scoreAnimation3, start = 1, target = 0.75, duration = 500,
                    callback = function(value)
                        circle.outline.xScale = value; circle.outline.yScale = value
                        circle.shape.xScale = value; circle.shape.yScale = value 
                    end,
                    finalCallback = function(value)
                        circle.scoreAnimation4 = animator:animate({
                            id = circle.scoreAnimation4, start = 0.75, target = 1, duration = 300,
                            callback = function(value)
                                circle.outline.xScale = value; circle.outline.yScale = value
                                circle.shape.xScale = value; circle.shape.yScale = value
                            end
                        })
                    end
                })
            end
        end
    end
end


-- particles not used
local frames = 0
local factor = 0
function onUpdateScoreBar()
    frames = frames + 1
    factor = 60 / PARTICLES_PER_SECOND
    for name, circle in pairs(circles) do
        circle.shape.rotation = circle.shape.rotation + CIRCLE_MAX_ROTATION_SPEED * normalize(-20, CIRCLE_MAX_ROTATION_SPEED, circle.score)
        --circle.shape.fill.a = 0.33
        if circle.score >= 3 then
            local particles = circle.score / factor / 2
            if  frames % factor == 0 then
                for i = 1, particles, 1 do
                    local particleSpeedFactor = normalize(0, 1, circle.score)
                    local velX = -1 * math.random(1, particleSpeedFactor) --(-20 * particleSpeedFactor) - math.random( -PARTICLE_SPARK_RANDOM_RANGE * particleSpeedFactor, PARTICLE_SPARK_RANDOM_RANGE * particleSpeedFactor )
                    local velY = 100
                    local life = 0.5 --0.25 * particleSpeedFactor / 2
                    local sparkColor = { 1, 1, 1 }

                    if circle.type == CIRCLE_TYPE_RED then sparkColor = paint.rainbow.red
                    elseif circle.type == CIRCLE_TYPE_ORANGE then sparkColor = paint.rainbow.orange
                    elseif circle.type == CIRCLE_TYPE_YELLOW then sparkColor = paint.rainbow.yellow
                    elseif circle.type == CIRCLE_TYPE_GREEN then sparkColor = paint.rainbow.green
                    elseif circle.type == CIRCLE_TYPE_AZURE then sparkColor = paint.rainbow.azure
                    elseif circle.type == CIRCLE_TYPE_BLUE then sparkColor = paint.rainbow.blue
                    elseif circle.type == CIRCLE_TYPE_VIOLET then sparkColor = paint.rainbow.violet
                    end

                    particleSystem:createParticle({
                        flags = { "water" },
                        x = circle.x - circle.shape.width / 2 - 1,
                        y = circle.y,
                        velocityX = velX,
                        velocityY = velY,
                        color = sparkColor,
                        lifetime = life
                    })
                end
                for i = 1, particles * 0.5, 1 do
                    local particleSpeedFactor = normalize(0, 1, circle.score)
                    local velX = -1 * math.random(1, particleSpeedFactor)
                    local velY = -100
                    local life = 1
                    local smokeColor = math.random(0.1, 0.3)
                    particleSystem:createParticle({
                        flags = { "water" },
                        x = circle.x + circle.shape.width / 2 + 1,
                        y = circle.y,
                        velocityX = velX,
                        velocityY = velY,
                        color = { 0.2, 0.2, 0.2 },
                        lifetime = life
                    })
                end
            end
        end
    end
    if frames > 60 then frames = 0 end
end