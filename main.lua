-- @Task локализировать библиотечные вызовы (math)
-- @Task добавить прыгающие круги на фоне
require("utils")
require("CircleManager")
require("ScorebarManager")

FONT                        = "fonts\\iosevka"
IMAGE_SPARK                 = "images\\spark.png"
IMAGE_XMARK                 = "images\\xmark.png"

SOUND_TAP_1                 = audio.loadSound("sounds\\tap_1.mp3")
SOUND_TAP_2                 = audio.loadSound("sounds\\tap_2.mp3")
SOUND_ERROR                 = audio.loadSound("sounds\\error.mp3")
SOUND_IDK                   = audio.loadSound("sounds\\idk.mp3")
SOUND_RESTRICTED            = audio.loadSound("sounds\\restricted.mp3")
SOUND_MAGICK                = audio.loadSound("sounds\\magick.mp3")
SOUND_WASTED                = audio.loadSound("sounds\\wasted.mp3")
SOUND_WHIP                  = audio.loadSound("sounds\\whip.mp3")
SOUND_THE_GRAND_CATHEDRAL   = audio.loadStream("sounds\\the_grand_cathedral.mp3")
SOUND_PIZZA                 = audio.loadStream("sounds\\pizza.mp3")
SOUND_CRYSTALIZE            = audio.loadStream("sounds\\crystalize.mp3")

SCENE_WELCOME       = "scenes.welcome"
SCENE_MENU          = "scenes.menu"
SCENE_PREPARE       = "scenes.prepare"
SCENE_GAME          = "scenes.game"

GAMEMODE_LEVELS = {}
GAMEMODE_LEVELS[1] = { title = "Разогрев",                  score = 5,  speed = 5, circles = 1, specialCircles = 0, routes = 1 }
GAMEMODE_LEVELS[2] = { title = "Стрельба по тарелочкам",    score = 10, speed = 5, circles = 2, specialCircles = 0, routes = 1 }
GAMEMODE_LEVELS[3] = { title = "Третий - не лишний",        score = 35, speed = 4, circles = 3, specialCircles = 2, routes = 2 }
GAMEMODE_LEVELS[4] = { title = "Легкий трафик",             score = 50, speed = 3, circles = 4, specialCircles = 4, routes = 2 }
GAMEMODE_LEVELS[5] = { title = "Радуга 6",                  score = 75, speed = 3, circles = 6, specialCircles = 6, routes = 3 }
GAMEMODE_LEVELS[6] = { title = "The Grand Cathedral",       score = 360, speed = 2, circles = 21, specialCircles = 50, routes = 5 }
GAMEMODE_LEVELS[7] = { title = "#!№@+#@!!$",                score = 120, speed = 6, circles = 7, specialCircles = 30, routes = 5 }
GAMEMODE_LEVELS_COUNT = 8

CIRCLE_TYPE_RED         = 1
CIRCLE_TYPE_ORANGE      = 2
CIRCLE_TYPE_YELLOW      = 3
CIRCLE_TYPE_GREEN       = 4
CIRCLE_TYPE_AZURE       = 5
CIRCLE_TYPE_BLUE        = 6
CIRCLE_TYPE_VIOLET      = 7
CIRCLE_TYPE_SPECIAL     = 8
CIRCLE_RADIUS           = display.contentWidth / 20
CIRCLE_MIN_DISTANCE_X   = 150
CIRCLE_MIN_DISTANCE_Y   = 300

BACKGROUND_CIRCLES_COUNT = 256
BACKGROUND_CIRCLES_SIZE = 10
BACKGROUND_CIRCLES_SPEED = 0.5

SCOREBAR_CIRCLE_SHAPE_ALPHA = 0.5

audio.setVolume(0.2)
audio.setVolume(0.2, { channel = 2 })

math.randomseed(os.clock())
animator:init()
local composer = {}
local background = {}
local backgroundCircles = {}
local backgroundCirclesCount = 0
local backgroundCirclesSpeedFactor = 0.2
backgroundCirclesSpeedFactor2 = 1

player = {}
player.level = 1
player.score = 0
player.isDone = false
player.isWon = false
player.isNextLevel = false


------------------------------------------------------------


function generateStartLocation(radius)
    local min = { x = radius, y = radius }
    local max = { x = screen.size.w - radius, y = screen.size.h - radius }
    return { x = math.random(min.x, max.x), y = math.random(min.y, max.y) }
end

function generateTargetLocation(startLocation, radius, minDistX, minDistY)
    if (startLocation == nil) then startLocation = { x = screen.center.x, y = screen.center.y } end
    if (radius == nil) then radius = 20 end
    if (minDistX == nil) then minDistX = 20 end
    if (minDistY == nil) then minDistY = 40 end
    local min = { x = radius, y = radius }
    local max = { x = screen.size.w - radius, y = screen.size.h - radius }
    local target = { x = 0, y = 0}
    local done = { x = false, y = false}
    while not done.x or not done.y do
        if not done.x then target.x = math.random(min.x, max.x) end
        if not done.y then target.y = math.random(min.y, max.y) end
        if target.x - startLocation.x > minDistX or target.x - startLocation.x < -minDistX then done.x = true end
        if target.y - startLocation.y > minDistY or target.y - startLocation.y < -minDistY then done.y = true end
    end
    return target
end

local function createBackgroundCircle(radius)
    local circle = display.newGroup()
    circle.shape = display.newCircle(circle, 0, 0, radius)
    circle.shape:setFillColor(0.33, 0.33, 0.33, 0.1)

    local location = generateStartLocation(radius)
    circle.x = location.x
    circle.y = location.y
    circle.start = location
    circle.target = generateTargetLocation(location)
    return circle
end

local function moveBackgrounCircles()
    local speed = BACKGROUND_CIRCLES_SPEED * backgroundCirclesSpeedFactor * backgroundCirclesSpeedFactor2
    local xpos = screen.center.x
    local ypos = screen.center.y
    local isPositiveDiffX = 0
    local isPositiveDiffY = 0
    local diff = {}
    for i = 1, backgroundCirclesCount, 1 do
        local circle = backgroundCircles[i]
        diff.x  = circle.target.x - circle.start.x
        diff.y  = circle.target.y - circle.start.y
        isPositiveDiffX = diff.x > 0
        isPositiveDiffY = diff.y > 0

        if isPositiveDiffX and isPositiveDiffY and math.abs(diff.x) > math.abs(diff.y) then
            -- move right and down OK
            xpos = circle.x + speed
            ypos = circle.start.y + (circle.target.y - circle.start.y) * normalize(circle.start.x, circle.target.x, circle.x)
        elseif isPositiveDiffX and isPositiveDiffY and math.abs(diff.x) < math.abs(diff.y) then
            -- move down and right OK
            ypos = circle.y + speed
            xpos = circle.start.x + (circle.target.x - circle.start.x) * normalize(circle.start.y, circle.target.y, circle.y)
        elseif isPositiveDiffX and not isPositiveDiffY and math.abs(diff.x) > math.abs(diff.y) then
            -- move right and up OK
            xpos = circle.x + speed
            ypos = circle.start.y + (circle.target.y - circle.start.y) * normalize(circle.start.x, circle.target.x, circle.x)
        elseif isPositiveDiffX and not isPositiveDiffY and math.abs(diff.x) < math.abs(diff.y) then
            -- move up and right OK
            ypos = circle.y - speed
            xpos = circle.start.x + (circle.target.x - circle.start.x) * normalize(circle.start.y, circle.target.y, circle.y)
        elseif not isPositiveDiffX and isPositiveDiffY and math.abs(diff.x) > math.abs(diff.y) then
            -- move left and down OK
            xpos = circle.x - speed
            ypos = circle.start.y + (circle.target.y - circle.start.y) * normalize(circle.start.x, circle.target.x, circle.x)
        elseif not isPositiveDiffX and isPositiveDiffY and math.abs(diff.x) < math.abs(diff.y) then
            -- move down and left OK
            ypos = circle.y + speed
            xpos = circle.start.x + (circle.target.x - circle.start.x) * normalize(circle.start.y, circle.target.y, circle.y)
        elseif not isPositiveDiffX and not isPositiveDiffY and math.abs(diff.x) > math.abs(diff.y) then
            -- move left and up OK
            xpos = circle.x - speed
            ypos = circle.start.y + (circle.target.y - circle.start.y) * normalize(circle.start.x, circle.target.x, circle.x)
        elseif not isPositiveDiffX and not isPositiveDiffY and math.abs(diff.x) < math.abs(diff.y) then
            -- move up and left OK
            ypos = circle.y - speed
            xpos = circle.start.x + (circle.target.x - circle.start.x) * normalize(circle.start.y, circle.target.y, circle.y)
        end

        circle.x = xpos
        circle.y = ypos

        if ((isPositiveDiffX and circle.x >= circle.target.x) or (not isPositiveDiffX and circle.x <= circle.target.x)) and
        ((isPositiveDiffY and circle.y >= circle.target.y) or (not isPositiveDiffY and circle.y <= circle.target.y))
        then
            circle.start = circle.target
            circle.target = generateTargetLocation(circle.start, BACKGROUND_CIRCLES_SIZE, 0)
        end
    end
end


------------------------------------------------------------


background = display.newRect(screen.center.x, screen.center.y, screen.size.w, screen.size.h)
for i = 1, BACKGROUND_CIRCLES_COUNT, 1 do
    backgroundCirclesCount = backgroundCirclesCount + 1
    backgroundCircles[backgroundCirclesCount] = createBackgroundCircle(BACKGROUND_CIRCLES_SIZE)
    backgroundCircles[backgroundCirclesCount].alpha = 0
end

animator:animate({ start = 0, target = 1, duration = 10000,
    callback = function(value)
        for i = 1, backgroundCirclesCount, 1 do
            backgroundCircles[i].alpha = value
        end
    end
})

animator:animate({
    start = 0.2, target = 3.0, duration = 1500, cyclic = true,
    callback = function(value) backgroundCirclesSpeedFactor = value end
})


------------------------------------------------------------


function update()
    moveBackgrounCircles()
end

composer = require("composer")
composer.recycleOnSceneChange = true
Runtime:addEventListener("enterFrame", update)
composer.gotoScene(SCENE_WELCOME, { effect = "fade", time = 600 })
audio.play(SOUND_CRYSTALIZE, { channel = 2 })