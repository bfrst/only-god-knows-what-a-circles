local composer = require("composer")

local TEXT_1 = "Добро пожаловать в Circles Game"
local TEXT_2 = "Enjoy!"

local scene = composer.newScene()
local textMessage = {}
local textEnjoy = {}


function scene:create(event)
    local sceneGroup = self.view
    audio.play(SOUND_WASTED)
    textMessage = display.newText(sceneGroup, TEXT_1, screen.center.x, screen.center.y, FONT, 24)
    textEnjoy = display.newText(sceneGroup, TEXT_2, screen.center.x, screen.center.y + 50, FONT, 32)
    textMessage:setFillColor(0.2, 0.2, 0.2)
    textEnjoy:setFillColor(0.2, 0.2, 0.2)
    textEnjoy.alpha = 0
    animator:animate({
        start = 0, target = 0, duration = 2300,
        callback = function(value) end,
        finalCallback = function(value)
            animator:animate({
                start = 0, target = 1, duration = 200,
                callback = function(value)
                    textEnjoy.alpha = value
                end
            })
            animator:animate({
                start = 5, target = 1, duration = 300,
                callback = function(value)
                    textEnjoy.xScale = value
                    textEnjoy.yScale = value
                end
            })
        end
    })
end

function scene:show(event)
    local sceneGroup = self.view
    local phase = event.phase
    
    if (phase == "will") then
        -- when scene is off screen
    elseif (phase == "did") then
        animator:animate({
            start = 0, target = 0, duration = 3000,
            callback = function(value) end,
            finalCallback = function(value)
                composer.gotoScene(SCENE_MENU, { effect = "slideLeft", time = 300 })
            end
        })
    end
end

function scene:hide(event)
    local sceneGroup = self.view
    local phase = event.phase
    if (phase == "will") then
        -- when scene is on the screen
    elseif (phase == "did") then
        -- when scene is off the screen
    end
end

function scene:destroy(event)
    local sceneGroup = self.view
end

scene:addEventListener("create", scene)
scene:addEventListener("show", scene)
scene:addEventListener("hide", scene)
scene:addEventListener("destroy", scene)

return scene