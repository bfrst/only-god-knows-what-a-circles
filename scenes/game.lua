local composer = require("composer")
local scene = composer.newScene()
local background = {}

function scene:create(event)
    local sceneGroup = self.view
    audio.play(SOUND_WHIP)

    if (event.phase == "will") then
    elseif (event.phase == "did") then
    end
end

function scene:show(event)
    local sceneGroup = self.view
    local phase = event.phase


    if (phase == "will") then
    elseif (phase == "did") then
        background = display.newRect(sceneGroup, screen.center.x, screen.center.y, screen.size.w, screen.size.h)
        background.fill = { 0, 0.25, 0.5, 0.1 }
        if player.level == 6 then
            audio.stop({ channel = 2 })
            audio.setVolume(0.5, { channel = 2 })
            audio.play(SOUND_THE_GRAND_CATHEDRAL, { channel = 2 })
            animator:animate({ start = 0.1, target = 0.75, duration = 60000,
                callback = function(value)
                    background.fill = { 0.1, 0.1, 0.1, value }
                end
            })
            animator:animate({ start = 1, target = 0.1, duration = 30000,
                callback = function(value)
                    backgroundCirclesSpeedFactor2 = value
                end
             })
        elseif player.level == 7 then
            audio.stop({ channel = 2 })
            audio.setVolume(0.2, { channel = 2 })
            audio.play(SOUND_PIZZA, { channel = 2 })
            backgroundCirclesSpeedFactor2 = 3
        end
        ScorebarManager:init()
        CircleManager:init()
        CircleManager:newLevel(player.level)
        ScorebarManager:newLevel(player.level)
        CircleManager:startGame()
        background:addEventListener("touch", function(event)
            if (event.phase == "began") then
                audio.play(SOUND_RESTRICTED)
            end
        end)
    end
end

function scene:hide(event)
    local sceneGroup = self.view
    local phase = event.phase

    if (phase == "will") then
    elseif (phase == "did") then
        --ScorebarManager:release()
    end
end

function scene:destroy(event)
    local sceneGroup = self.view
    local phase = event.phase

    if (phase == "will") then
    elseif (phase == "did") then
    end
end

local function winCondition()
    if (player.score >= GAMEMODE_LEVELS[player.level].score) then
        player.isNextLevel = true
        player.isDone = true
        player.level = player.level + 1
    elseif (player.score <= -GAMEMODE_LEVELS[player.level].score) then
        player.isDone = true
    end
    if player.isDone then
        audio.rewind({ channel = 1 })
        audio.stop({ channel = 1 })
        Runtime:removeEventListener("enterFrame", winCondition)
        ScorebarManager:release()
        CircleManager:endGame()
        CircleManager:release()
        player.score = 0
        backgroundCirclesSpeedFactor2 = 1
        composer.gotoScene(SCENE_PREPARE, { effect = "slideLeft", time = 300 })
    end
end

scene:addEventListener("create", scene)
scene:addEventListener("show", scene)
scene:addEventListener("hide", scene)
scene:addEventListener("destroy", scene)
Runtime:addEventListener("enterFrame", winCondition)

return scene