local composer = require("composer")
local scene = composer.newScene()
local levelText = {}
local levelTitle = {}

function scene:create(event)
    local sceneGroup = self.view
    local mainText = "Уровень "
    local secondText = ""
    if (player.level > GAMEMODE_LEVELS_COUNT) then
        player.isWon = true
        player.isNextLevel = false
    end
    if (player.isWon) then
        mainText = "Поздравляем!"
        secondText = "Вы победили!"
    elseif player.isDone and not player.isNextLevel then
        mainText = "К сожалению"
        secondText = "Вы проиграли"
    else
        mainText = "Уровень " .. player.level
        secondText = GAMEMODE_LEVELS[player.level].title
    end
    levelText = display.newText({
        parent = sceneGroup, x = screen.center.x, y = screen.center.y, width = screen.size.w / 2, font = FONT, fontSize = 32,
        align = "center", text = mainText
    })
    levelText:setFillColor(0.2, 0.2, 0.2)
    levelTitle = display.newText({
        parent = sceneGroup, x = screen.center.x, y = screen.center.y + 50, width = screen.size.w / 2, font = FONT, fontSize = 24,
        align = "center", text = secondText
    })
    levelTitle:setFillColor(0.2, 0.2, 0.2)
end

function scene:show(event)
    local sceneGroup = self.view

    if (event.phase == "did") then
        animator:animate({ duration = 2000, finalCallback = function()
            local toScene = SCENE_GAME
            if player.isDone and player.isNextLevel == false then 
                local data = ""
                local path = system.pathForFile("scores.txt", system.DocumentsDirectory)
                local file, errorString = io.open(path, "r")
                if file then
                    data = file:read("*a")
                    io.close(file)
                end
                file, errorString = io.open(path, "w")
                if file then
                    data = data .. "@" .. player.level
                    file:write(data)
                    io.close(file)
                end
                file = nil
                
                player.level = 1
                toScene = SCENE_MENU
            end

            player.isDone = false
            player.isWon = false
            player.isNextLevel = false
            composer.gotoScene(toScene, { effect = "slideLeft", time = 300 })
        end })
    end
end

function scene:hide(event)
    local sceneGroup = self.view
end

function scene:destroy(event)
    local sceneGroup = self.view
end

scene:addEventListener("create", scene)
scene:addEventListener("show", scene)
scene:addEventListener("hide", scene)
scene:addEventListener("destroy", scene)

return scene