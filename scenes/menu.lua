local composer = require("composer")
local scene = composer.newScene()
local buttonPlay = {}
local leaderboard = {}

-- @desc Creates a button with specified params
------------------------------------------------------------
-- @param opt backgroundColor
-- @param opt cornerRadius
-- @param opt x
-- @param opt y
-- @param opt width
-- @param opt height
-- @param opt text
-- @param opt textColor
-- @param opt textSize
-- @param opt textFont
-- @param opt clickCallback
function createButton(params)
    if (params.x == nil) then params.x = screen.center.x end
    if (params.y == nil) then params.y = screen.center.y end
    if (params.width == nil) then params.width = 200 end
    if (params.height == nil) then params.height = 50 end
    if (params.text == nil) then params.text = "" end
    if (params.textSize == nil) then params.textSize = 24 end
    if (params.textFont == nil) then params.textFont = "iosevka" end
    if (params.textColor == nil) then params.textColor = { 1, 1, 1 } end
    if (params.clickCallback == nil) then params.clickCallback = function() end end
    if (params.backgroundColor == nil) then params.backgroundColor = { 0.33, 0.33, 0.33} end
    if (params.cornerRadius == nil) then params.cornerRadius = 5 end

    local button = display.newGroup()
    button.background = display.newRoundedRect(button, params.x, params.y, params.width, params.height, params.cornerRadius)
    button.background.fill = params.backgroundColor
    button.text = display.newText(button, params.text, params.x, params.y, "iosevka", params.textSize)
    button.text:setFillColor(1,1,1)
    button.clickCallback = params.clickCallback
    button:addEventListener("touch", button.clickCallback)
    return button
end

-- @desc Creates a leaderboard with specified params
------------------------------------------------------------
-- @param opt backgroundAlpha
-- @param opt backgroundColor
-- @param opt cornerRadius
-- @param opt x
-- @param opt y
-- @param opt width
-- @param opt height
-- @param opt items = [TextObject("text"), TextObject("text")]
-- @param opt textColor
-- @param opt textSize
-- @param opt textFont
-- @param opt clickCallback
function createLeaderboard(params)
    if (params.x == nil) then params.x = screen.center.x end
    if (params.y == nil) then params.y = screen.center.y end
    if (params.width == nil) then params.width = screen.size.w - 50 end
    if (params.height == nil) then params.height = screen.size.h - params.y - 50 end
    if (params.items == nil) then params.items = {} end
    if (params.textSize == nil) then params.textSize = 24 end
    if (params.textFont == nil) then params.textFont = FONT end
    if (params.textColor == nil) then params.textColor = { 1, 1, 1 } end
    if (params.clickCallback == nil) then params.clickCallback = function() end end
    if (params.backgroundColor == nil) then params.backgroundColor = { 0.33, 0.33, 0.33} end
    if (params.backgroundAlpha == nil) then params.backgroundAlpha = 0.66 end
    if (params.cornerRadius == nil) then params.cornerRadius = 5 end

    local leaderboard = display.newGroup()
    leaderboard.x = params.x
    leaderboard.y = params.y
    leaderboard.background = display.newRoundedRect(leaderboard, 0, 0, params.width, params.height, params.cornerRadius)
    leaderboard.y = leaderboard.y + leaderboard.background.height / 2
    leaderboard.background.fill = params.backgroundColor
    leaderboard.background.alpha = params.backgroundAlpha
    if (#params.items == 0) then 
        leaderboard.item = display.newText({
            x = 0, y = 0, width = params.width, font = FONT, align = "center", fontSize = 24,
            text = "Здесь будет\nотображаться\nваша статистика\nза последние\n10 игр", parent = leaderboard
        })
    else
        leaderboard.title = display.newText({
            x = 0, y = -leaderboard.height / 2 + 30, font = FONT, align = "center", fontSize = 24,
            text = "Ваша статистика за последние 10 игр:", parent = leaderboard
        })
        local factor = leaderboard.height / 10 - 10
        leaderboard.item = {}
        local itemsCount = #params.items
        if (#params.items > 10) then itemsCount = 10 end
        local i = 1
        for item = #params.items, #params.items - itemsCount + 1, -1 do
            leaderboard.item[i] = display.newText({
                x = 0, y = -leaderboard.height / 2 + 30 + i * factor, font = FONT, align = "center", fontSize = 24,
                text = i .. ": Вы достигли " .. params.items[item] .. " уровня!", parent = leaderboard
            })
            leaderboard.item[i].alpha = 0
            i = i + 1
        end
    end
    return leaderboard
end
-- max level, 

function scene:create(event)
    local sceneGroup = self.view
    audio.play(SOUND_WHIP)
    buttonPlay = createButton({ text = "Играть", width = screen.size.w - 50, y =  50,
    clickCallback = function(event)
        if not (event.phase == "ended") then return end
        if not (event.target.isBlocked == nil) and event.target.isBlocked then return end
        event.target.isBlocked = true
        audio.play(SOUND_TAP_2)
        animator:animate({ start = 1, target = 1.25, duration = 200,
        callback = function(value)
            buttonPlay.text.xScale = value; buttonPlay.yScale = value
        end,
        finalCallback = function(value)
            animator:animate({ start = 1.25, target = 1, duration = 400,
            callback = function(value)
                buttonPlay.text.xScale = value; buttonPlay.yScale = value
            end,
            finalCallback = function(value)
                audio.play(SOUND_WHIP)
                event.target.isBlocked = false
                composer.gotoScene(SCENE_PREPARE, { effect = "slideLeft", time = 300 })
            end
            })
        end
        })
    end
    })
    sceneGroup:insert(buttonPlay)
    -- @Task read from the file scoreboard
    local path = system.pathForFile("scores.txt", system.DocumentsDirectory)
    local file, errorString = io.open(path, "r")
    local content = ""
    local scoreItems = {}
    local scoreItemsCount = 0
    if not file then
        print("File error: " .. errorString)
    else
        content = file:read("*a")
        io.close(file)
    end
    if content then
        local done = false
        local nextItem = nil
        while not done do
            if nextItem then
                nextItem = string.find(content, "@", nextItem + 1)
            else
                nextItem = string.find(content, "@")
            end
            if (nextItem == nil) then
                done = true
            else
                scoreItemsCount = scoreItemsCount + 1
                scoreItems[scoreItemsCount] = string.sub(content, nextItem + 1, nextItem + 1)
            end
        end
    end
    leaderboard = createLeaderboard({ y = 100, items = scoreItems }) --"32", "41", "1", "14", "18", "27", "61", "18", "27", "61"
    sceneGroup:insert(leaderboard)
    if (phase == "will") then
    elseif (phase == "did") then
    end
end

function scene:show(event)
    local sceneGroup = self.view
    local phase = event.phase

    if (phase == "will") then
    elseif (phase == "did") then
        for i = 1, #leaderboard.item, 1 do
            animator:animate({ start = 0, target = 1, duration = 200 + i * 200, callback = function(value)
                leaderboard.item[i].alpha = value
            end
            })
        end
    end
end

function scene:hide(event)
    local sceneGroup = self.view
    local phase = event.phase

    if (phase == "will") then
    elseif (phase == "did") then
    end
end

function scene:destroy(event)
    local sceneGroup = self.view
    local phase = event.phase

    if (phase == "will") then
    elseif (phase == "did") then
    end
end

scene:addEventListener("create", scene)
scene:addEventListener("show", scene)
scene:addEventListener("hide", scene)
scene:addEventListener("destroy", scene)

return scene