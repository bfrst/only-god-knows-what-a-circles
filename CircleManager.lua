CircleManager = {}


-- @desc Initializes circle manager
------------------------------------------------------------
-- @param opt boundaries = { x1, x2, y1, y2 }
-- @param opt routeDistance = { x = minValue, y = minValue }
-- @param opt level
-- @param opt score
-- @param opt speed
-- @param opt circleCount
-- @param opt specialCircles
-- @param opt routes
-- @param opt spawnRate
function CircleManager:init(params)
    if not (self.isInitialized == nil) and self.isInitialized then return end
    self.isInitialized = true
    if (params == nil) then params = {} end
    if (params.boundaries == nil) then 
        params.boundaries = {
            x1 = CIRCLE_RADIUS, x2 = screen.size.w - CIRCLE_RADIUS,
            y1 = 200, y2 = screen.size.h - CIRCLE_RADIUS
        }
    end

    if (params.routeDistance == nil) then
        params.routeDistance = { x = 150, y = 300 }
    end

    self.circles = {}
    self.circlesCount = 0
    self.specialCircles = {}
    self.specialCirclesCount = 0
    self.boundaries = params.boundaries
    self.routeDistance = params.routeDistance
    self.level = {
        scoreRequired = 0,
        speedFactor = 0,
        circlesCount = 0,
        specialCirclesCount = 0,
        routesCount = 0,
        spawnRate = 0
    }
end

function CircleManager:release()
    self.isInitialized = false
end

function CircleManager:newLevel(levelId)
    local gamemode = GAMEMODE_LEVELS[levelId]
    self.level.scoreRequired = gamemode.score
    self.level.speedFactor = gamemode.speed
    self.level.circlesCount = gamemode.circles
    self.level.specialCirclesCount = gamemode.specialCircles
    self.level.routesCount = gamemode.routes
    self.level.spawnRate = gamemode.spawnRate
end

function CircleManager:genStartLocation(index, isSpecial)
    local loc = {
        x = math.random(self.boundaries.x1, self.boundaries.x2),
        y = math.random(self.boundaries.y1, self.boundaries.y2)
    }
    local circle = self.circles[index]
    if not (isSpecial == nil) and isSpecial then circle = self.specialCircles[index] end
    circle.start = loc
    circle.x = loc.x
    circle.y = loc.y
end

function CircleManager:genTargetLocation(index, isSpecial)
    local circle = self.circles[index]
    if not (isSpecial == nil) and isSpecial then circle = self.specialCircles[index] end
    local done = { x = false, y = false }
    local result = { x = 0, y = 0 }
    while not done.x or not done.y do
        if not done.x then result.x = math.random(self.boundaries.x1, self.boundaries.x2) end
        if not done.y then result.y = math.random(self.boundaries.y1, self.boundaries.y2) end
        if result.x - circle.start.x > self.routeDistance.x or result.x - circle.start.x < -self.routeDistance.x then
            done.x = true end
        if result.y - circle.start.y > self.routeDistance.y or result.y - circle.start.y < -self.routeDistance.y then
            done.y = true end
    end
    circle.target = result
end

local function handleTouch(event)
    if not (event.phase == "began") then return end
    if (event.target.isBlocked) then return end
    audio.play(SOUND_TAP_1)
    local circle = event.target
    circle.isBlocked = true
    if not (circle.type == CIRCLE_TYPE_SPECIAL) then
        circle.touchAnimation1 = animator:animate({
            start = circle.outline.xScale, target = 1.5, duration = 200,
            callback = function(value)
                circle.outline.xScale = value
                circle.outline.yScale = value
            end,
            finalCallback = function(value)
                circle.touchAnimation2 = animator:animate({
                    start = circle.shape.xScale, target = 0.01, duration = 200,
                    callback = function(value)
                        circle.shape.xScale = value
                        circle.shape.yScale = value
                    end
                })
                circle.touchAnimation3 = animator:animate({
                    start = 1.5, target = 0.01, duration = 300,
                    callback = function(value)
                        circle.outline.xScale = value
                        circle.outline.yScale = value
                    end,
                    finalCallback = function(value)
                        if not (CircleManager.isInitialized) then return end
                        CircleManager:respawnCircle(circle.id)
                    end
                })
            end
        })
    else
        animator:animate({
            start = 1, target = 5, duration = 600,
            callback = function(value)
                circle.outline.xScale = value
                circle.outline.yScale = value
            end
        })
        animator:animate({
            start = 1, target = 0.01, duration = 700,
            callback = function(value)
                circle.alpha = value
            end,
            finalCallback = function(value)
                circle.isVisible = false
            end
        })
    end
    
    local score = 1
    if (circle.type == CIRCLE_TYPE_SPECIAL) then score = 10 end
    ScorebarManager:addScore(event.target.type, score)
end

function CircleManager:moveCircle(index, isSpecial)
    local circle = self.circles[index]
    if not (isSpecial == nil) and (isSpecial) then circle = self.specialCircles[index] end
    local speed = CircleManager.level.speedFactor
    local xpos = screen.center.x
    local ypos = screen.center.y
    local diff = { x = circle.target.x - circle.start.x, y = circle.target.y - circle.start.y }
    local isPositiveDiffX = diff.x > 0
    local isPositiveDiffY = diff.y > 0

    if isPositiveDiffX and isPositiveDiffY and math.abs(diff.x) > math.abs(diff.y) then
        -- move right and down
        xpos = circle.x + speed * circle.speedFactor
        ypos = circle.start.y + (circle.target.y - circle.start.y) * normalize(circle.start.x, circle.target.x, circle.x)
    elseif isPositiveDiffX and isPositiveDiffY and math.abs(diff.x) < math.abs(diff.y) then
        -- move down and right
        ypos = circle.y + speed * circle.speedFactor
        xpos = circle.start.x + (circle.target.x - circle.start.x) * normalize(circle.start.y, circle.target.y, circle.y)
    elseif isPositiveDiffX and not isPositiveDiffY and math.abs(diff.x) > math.abs(diff.y) then
        -- move right and up
        xpos = circle.x + speed * circle.speedFactor
        ypos = circle.start.y + (circle.target.y - circle.start.y) * normalize(circle.start.x, circle.target.x, circle.x)
    elseif isPositiveDiffX and not isPositiveDiffY and math.abs(diff.x) < math.abs(diff.y) then
        -- move up and right
        ypos = circle.y - speed * circle.speedFactor
        xpos = circle.start.x + (circle.target.x - circle.start.x) * normalize(circle.start.y, circle.target.y, circle.y)
    elseif not isPositiveDiffX and isPositiveDiffY and math.abs(diff.x) > math.abs(diff.y) then
        -- move left and down
        xpos = circle.x - speed * circle.speedFactor
        ypos = circle.start.y + (circle.target.y - circle.start.y) * normalize(circle.start.x, circle.target.x, circle.x)
    elseif not isPositiveDiffX and isPositiveDiffY and math.abs(diff.x) < math.abs(diff.y) then
        -- move down and left
        ypos = circle.y + speed * circle.speedFactor
        xpos = circle.start.x + (circle.target.x - circle.start.x) * normalize(circle.start.y, circle.target.y, circle.y)
    elseif not isPositiveDiffX and not isPositiveDiffY and math.abs(diff.x) > math.abs(diff.y) then
        -- move left and up
        xpos = circle.x - speed * circle.speedFactor
        ypos = circle.start.y + (circle.target.y - circle.start.y) * normalize(circle.start.x, circle.target.x, circle.x)
    elseif not isPositiveDiffX and not isPositiveDiffY and math.abs(diff.x) < math.abs(diff.y) then
        -- move up and left
        ypos = circle.y - speed * circle.speedFactor
        xpos = circle.start.x + (circle.target.x - circle.start.x) * normalize(circle.start.y, circle.target.y, circle.y)
    end

    circle.x = xpos
    circle.y = ypos

    if ((isPositiveDiffX and circle.x >= circle.target.x) or (not isPositiveDiffX and circle.x <= circle.target.x)) and
    ((isPositiveDiffY and circle.y >= circle.target.y) or (not isPositiveDiffY and circle.y <= circle.target.y))
    then
        circle.routesDone = circle.routesDone + 1
        if circle.routesDone > CircleManager.level.routesCount then
            CircleManager:killCircle(circle.id, isSpecial)
        else
            CircleManager:growCircle(circle.id, isSpecial)
        end
    end
end

function CircleManager:growCircle(index, isSpecial)
    local circle = self.circles[index]
    if not (isSpecial == nil) and (isSpecial) then circle = self.specialCircles[index] end
    local animId = nil
    if not (circle.animation == nil) then animId = circle.animation end
    circle.animation = animator:animate({
        id = animId, start = circle.shape.xScale, target = circle.shape.xScale - circle.growFactor, duration = 500,
        callback = function(value)
            circle.shape.xScale = value
            circle.shape.yScale = value
        end
    })
    circle.start = circle.target
    local special = false
    if not (isSpecial == nil) and isSpecial then special = true end
    CircleManager:genTargetLocation(circle.id, special)
end

function CircleManager:spawnCircle(index)
    local circle = self.circles[index]
    circle.shape.xScale = 0.01; circle.shape.yScale = 0.01
    circle.outline.xScale = 0.01; circle.shape.xScale = 0.01
    circle.outline.alpha = 1
    circle.shape.alpha = 1
    circle.alpha = 1
    animator:animate({
        start = 0, target = 1, duration = 100,
        callback = function(value) circle.alpha = value end,
        finalCallback = function(value) circle.alpha = value end
    })
    animator:animate({
        start = 0.01, target = 1.33, duration = 300,
        callback = function(value)
            circle.outline.xScale = value; circle.outline.yScale = value
        end,
        finalCallback = function(value)
            circle.outline.xScale = value; circle.outline.yScale = value
            animator:animate({
                start = 1.25, target = 0.8, duration = 300,
                callback = function(value) circle.outline.xScale = value; circle.outline.yScale = value end,
                finalCallback = function(value)
                    animator:animate({
                        start = 0.8, target = 1, duration = 200,
                        callback = function(value) circle.outline.xScale = value; circle.outline.yScale = value end,
                        finalCallback = function(value) circle.outline.xScale = value; circle.outline.yScale = value end
                    })
                end
            })
        end
    })
    animator:animate({
        start = 0.01, target = 1.25, duration = 300,
        callback = function(value)
            circle.shape.xScale = value; circle.shape.yScale = value
        end,
        finalCallback = function(value)
            circle.shape.xScale = value; circle.shape.yScale = value
            animator:animate({
                start = 1.25, target = 0.5, duration = 300, 
                callback = function(value) circle.shape.xScale = value; circle.shape.yScale = value end,
                finalCallback = function(value)
                    circle.shape.xScale = value; circle.shape.yScale = value
                    animator:animate({
                        start = 0.5, target = 1, duration = 200,
                        callback = function(value) circle.shape.xScale = value; circle.shape.yScale = value end,
                        finalCallback = function(value)
                            circle.shape.xScale = value; circle.shape.yScale = value
                            circle.isBlocked = false
                        end
                    })
                end
            })
        end
    })
end

function CircleManager:spawnSpecialCircle(index)
    local circle = self.specialCircles[index]
    circle.shape.xScale = 0.01; circle.shape.yScale = 0.01
    circle.outline.xScale = 0.01; circle.shape.xScale = 0.01
    animator:animate({
        start = 0, target = 1, duration = 100,
        callback = function(value) circle.alpha = value end,
        finalCallback = function(value) circle.alpha = value end
    })
    animator:animate({
        start = 0.01, target = 1.33, duration = 300,
        callback = function(value)
            circle.outline.xScale = value; circle.outline.yScale = value
        end,
        finalCallback = function(value)
            circle.outline.xScale = value; circle.outline.yScale = value
            animator:animate({
                start = 1.25, target = 0.8, duration = 300,
                callback = function(value) circle.outline.xScale = value; circle.outline.yScale = value end,
                finalCallback = function(value)
                    animator:animate({
                        start = 0.8, target = 1, duration = 200,
                        callback = function(value) circle.outline.xScale = value; circle.outline.yScale = value end,
                        finalCallback = function(value) circle.outline.xScale = value; circle.outline.yScale = value end
                    })
                end
            })
        end
    })
    animator:animate({
        start = 0.01, target = 1.25, duration = 300,
        callback = function(value)
            circle.shape.xScale = value; circle.shape.yScale = value
        end,
        finalCallback = function(value)
            circle.shape.xScale = value; circle.shape.yScale = value
            animator:animate({
                start = 1.25, target = 0.5, duration = 300, 
                callback = function(value) circle.shape.xScale = value; circle.shape.yScale = value end,
                finalCallback = function(value)
                    circle.shape.xScale = value; circle.shape.yScale = value
                    animator:animate({
                        start = 0.5, target = 1, duration = 200,
                        callback = function(value) circle.shape.xScale = value; circle.shape.yScale = value end,
                        finalCallback = function(value)
                            circle.shape.xScale = value; circle.shape.yScale = value
                            circle.isBlocked = false
                        end
                    })
                end
            })
        end
    })
end

function CircleManager:killCircle(index, isSpecial)
    audio.play(SOUND_IDK)
    local circle = self.circles[index]
    if not (isSpecial == nil) and (isSpecial) then circle = self.specialCircles[index] end
    circle.isBlocked = true
    circle.routesRepeated = 0
    circle.deadMark.isVisible = true
    circle.deadMark.alpha = 0

    ScorebarManager:addScore(circle.type, -1)
    if not isSpecial then
        animator:animate({
            start = 0, target = 1, duration = 250,
            callback = function(value)
                circle.deadMark.alpha = value
                circle.shape.alpha = 1 - value
                circle.outline.alpha = 1 - value
            end
        })
        animator:animate({
            start = 3, target = 1, duration = 250,
            callback = function(value)
                circle.deadMark.xScale = value
                circle.deadMark.yScale = value
            end,
            finalCallback = function(value)
                animator:animate({
                    start = 1, target = 0.01, duration = 250,
                    callback = function(value)
                        circle.deadMark.xScale = value
                        circle.deadMark.yScale = value
                        circle.alpha = value
                    end,
                    finalCallback = function(value)
                        circle.deadMark.isVisible = false
                        self:respawnCircle(circle.id)
                    end
                })
            end
        })
    else
        animator:animate({
            start = 1, target = 5, duration = 600,
            callback = function(value)
                circle.outline.xScale = value
                circle.outline.yScale = value
            end
        })
        animator:animate({
            start = 1, target = 0.01, duration = 700,
            callback = function(value)
                circle.alpha = value
            end
        })
    end
end

function CircleManager:respawnCircle(index)
    local circle = self.circles[index]
    circle.routesDone = 0
    self:genStartLocation(circle.id)
    self:genTargetLocation(circle.id)
    self:spawnCircle(circle.id)
end

function CircleManager:createCircle(circleType)
    self.circlesCount = self.circlesCount + 1
    self.circles[self.circlesCount] = display.newGroup()
    local circle = self.circles[self.circlesCount]

    circle.alpha = 0
    circle.id = self.circlesCount
    circle.isBlocked = true
    circle.speedFactor = 1
    circle.growFactor = (1 - 0.4) / self.level.routesCount
    circle.routesDone = 0
    
    self:genStartLocation(circle.id)
    self:genTargetLocation(circle.id)
    
    circle.outline = display.newCircle(circle, 0, 0, CIRCLE_RADIUS)
    circle.shape = display.newCircle(circle, 0, 0, CIRCLE_RADIUS - 3)
    circle.deadMark = display.newImage(circle, IMAGE_XMARK)
    circle.deadMark.width = 50
    circle.deadMark.height = 50
    circle.deadMark.isVisible = false

    circle.outline.fill = paint.main.black
    local factor = math.floor(circleType / 7)
    local type = circleType - 6 * factor
    if type == 1 or type == 8 then
        circle.shape.fill = FadeGradient(paint.rainbow.red)
        circle.type = CIRCLE_TYPE_RED
    elseif type == 2 then
        circle.shape.fill = FadeGradient(paint.rainbow.orange)
        circle.type = CIRCLE_TYPE_ORANGE
    elseif type == 3 then
        circle.shape.fill = FadeGradient(paint.rainbow.yellow)
        circle.type = CIRCLE_TYPE_YELLOW
    elseif type == 4 then
        circle.shape.fill = FadeGradient(paint.rainbow.green)
        circle.type = CIRCLE_TYPE_GREEN
    elseif type == 5 then
        circle.shape.fill = FadeGradient(paint.rainbow.azure)
        circle.type = CIRCLE_TYPE_AZURE
    elseif type == 6 then
        circle.shape.fill = FadeGradient(paint.rainbow.blue)
        circle.type = CIRCLE_TYPE_BLUE
    elseif type == 7 then
        circle.shape.fill = FadeGradient(paint.rainbow.violet)
        circle.type = CIRCLE_TYPE_VIOLET
    end

    circle:addEventListener("touch", handleTouch)
    return self.circlesCount
end

function CircleManager:createSpecialCircle()
    self.specialCirclesCount = self.specialCirclesCount + 1
    self.specialCircles[self.specialCirclesCount] = display.newGroup()
    local circle = self.specialCircles[self.specialCirclesCount]

    circle.id = self.specialCirclesCount
    circle.type = CIRCLE_TYPE_SPECIAL
    circle.isBlocked = true
    circle.speedFactor = 1.5
    circle.growFactor = (1 - 0.3) / self.level.routesCount
    circle.routesDone = 0
    circle.alpha = 0

    self:genStartLocation(circle.id, true)
    self:genTargetLocation(circle.id, true)

    circle.outline = display.newCircle(circle, 0, 0, CIRCLE_RADIUS)
    circle.shape = display.newCircle(circle, 0, 0, CIRCLE_RADIUS - 10)
    circle.deadMark = display.newImage(circle, IMAGE_XMARK)
    circle.deadMark.width = 50
    circle.deadMark.height = 50
    circle.deadMark.isVisible = false

    circle.shape.fill = paint.main.shadow
    local color = math.random(1, 7)
    if color == 1 then
        circle.outline.fill = FadeGradient(paint.rainbow.red)
    elseif color == 2 then
        circle.outline.fill = FadeGradient(paint.rainbow.orange)
    elseif color == 3 then
        circle.outline.fill = FadeGradient(paint.rainbow.yellow)
    elseif color == 4 then
        circle.outline.fill = FadeGradient(paint.rainbow.green)
    elseif color == 5 then
        circle.outline.fill = FadeGradient(paint.rainbow.azure)
    elseif color == 6 then
        circle.outline.fill = FadeGradient(paint.rainbow.blue)
    elseif color == 7 then
        circle.outline.fill = FadeGradient(paint.rainbow.violet)
    end
    circle:addEventListener("touch", handleTouch)
    return self.specialCirclesCount
end

-- @desc if no params passed - removes last circle
-- @param opt index
-- @param out type
function CircleManager:releaseCircle(index)
    if not (index == nil) then
        self.circles[index]:removeEventListener("touch", handleTouch)
        display.remove(self.circles[index])
        self.circles[index] = nil
    end
end

function CircleManager:releaseSpecialCircle(index)
    if not (index == nil) then
        self.specialCircles[index]:removeEventListener("touch", handleTouch)
        display.remove(self.specialCircles[index])
        self.specialCircles[index] = nil
    end
end

local function update()
    local circle = nil
    for i = 1, CircleManager.circlesCount, 1 do
        circle = CircleManager.circles[i]
        if not (circle.isBlocked) then
            CircleManager:moveCircle(circle.id)
        end
    end
    for i = 1, CircleManager.specialCirclesCount, 1 do
        circle = CircleManager.specialCircles[i]
        if not (circle.isBlocked) then
            CircleManager:moveCircle(circle.id, true)
        end
    end
end

local circleSpawnerId = 0
local specialCircleSpawnerId = 0
function CircleManager:startGame()
    Runtime:addEventListener("enterFrame", update)
    for i = 1, self.level.circlesCount, 1 do
        self:createCircle(i)
        circleSpawnerId = animator:animate({
            duration = 500 + i * 300, finalCallback = function(value) self:spawnCircle(i) end
        })
    end
    for i = 1, self.level.specialCirclesCount, 1 do
        self:createSpecialCircle()
        specialCircleSpawnerId = animator:animate({
            duration  = 500 + i * 6000, finalCallback = function(value) self:spawnSpecialCircle(i) end
        })
    end
end

function CircleManager:endGame()
    Runtime:removeEventListener("enterFrame", update)
    animator:release(circleSpawnerId)
    animator:release(specialCircleSpawnerId)
    for i = 1, self.circlesCount, 1 do
        CircleManager:releaseCircle(i)
    end
    for i = 1, self.level.specialCirclesCount, 1 do
        self:releaseSpecialCircle(i)
    end
end