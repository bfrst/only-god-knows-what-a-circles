screen = {
    center = { x = display.contentCenterX, y = display.contentCenterY },
    size = { w = display.contentWidth, h = display.contentHeight }
}

function normalize(start, target, current)
    return (current - start) / (target - start)
end

function RGB(red, green, blue)
    return { red / 255, green / 255, blue / 255 }
end

function FadeGradient(rgbObject, alpha)
    if (alpha == nil) then alpha = 0.75 end
    return {
        type = "gradient",
        color1 = { rgbObject[1], rgbObject[2], rgbObject[3], 1 },
        color2 = { rgbObject[1], rgbObject[2], rgbObject[3], alpha },
        direction = "down"
    }
end

paint = {
    shadow = {
        gradient = {
            type = "gradient",
            color1 = {0.1, 0.1, 0.1, 1},
            color2 = {0.1, 0.1, 0.1, 0.1},
            direction = "down"
        }
    },
    background = {
        main        = { 0.33, 0.33, 0.33 },
        blueGray    = { 0.3, 0.37, 0.44 }
    },
    rainbow = {
        red     = RGB(239, 83, 80),
        orange  = RGB(255, 112, 67),
        yellow  = RGB(255, 202, 40),
        green   = RGB(156, 204, 101),
        azure   = RGB(77, 208, 225),
        blue    = RGB(33, 150, 243),
        violet  = RGB(171, 71, 188)
    },
    main = {
        primary         = RGB(37, 51, 69),
        primaryDark     = RGB(27, 37, 49),
        primaryLight    = RGB(207, 216, 220),
        shadow          = RGB(21, 28, 38),
        accent          = RGB(78, 165, 217),
        acentDark       = RGB(58, 80, 108),
        devider         = RGB(189, 189, 189),
        grayText        = RGB(117, 117, 117),
        darkText        = RGB(21, 21, 21),
        white           = RGB(255, 255, 255),
        black           = RGB(0, 0, 0),
        accentGradient  = FadeGradient(RGB(78, 165, 217), 0.9),
        specialGradient = FadeGradient(RGB(77, 208, 225), 0.5),
    }
}

animator = {}

local function easeIn(params)
    local duration = params.duration
    local time = params.time
    local diff = params.diff
    local start = params.start
    local target = params.target
    local ct = time / duration
    return diff * ct * ct * ct + start
end

local function easeOut(params)
    local duration = params.duration
    local time = params.time
    local diff = params.diff
    local start = params.start
    local target = params.target
    local ct = time / duration - 1
    return -diff * (ct * ct * ct * ct - 1) + start
end

------------------------------------------
-- @desc reverses animation that was completed by passed id of animation
------------------------------------------
-- @param req id
-- @param opt overrideCallback
------------------------------------------
-- @return animation id
function animator:reverseAnimation(id)
    local start = self.animations[id].target
    local target = self.animations[id].start

    self.animations[id].start = start
    self.animations[id].target = target
    self.animations[id].diff = target - start
    self.animations[id].time = 0
    self.animations[id].completed = false
end

local FRAME_TIME = 16
local function update()
    local anim = nil
    for i = 1, animator.animationsCount, 1 do
        anim = animator.animations[i]
        if not (anim.isCompleted) then
            anim.time = anim.time + FRAME_TIME
            anim.value = easeOut({ duration = anim.duration, time = anim.time, diff = anim.diff, start = anim.start, target = anim.target })
            anim.callback(anim.value)
            if (anim.time >= anim.duration) then
                if not (anim.isCyclic) then 
                    anim.isCompleted = true
                    anim.finalCallback(anim.target)
                else
                    anim.finalCallback(anim.target)
                    animator:reverseAnimation(anim.id)
                end
            end
        end
    end
end

function animator:init()
    if not (self.isInitialized == nil) and self.isInitialized then return end
    self.isInitialized = true
    self.animationsCount = 0
    self.animations = {}
    Runtime:addEventListener("enterFrame", update)
end

-- @param opt id
-- @param opt duration
-- @param opt start
-- @param opt target
-- @param opt easing
-- @param opt isCyclic
-- @param opt callback
-- @param opt finalCallback
function animator:animate(params)
    self.animationsCount = self.animationsCount + 1

    if (params.duration == nil) then params.duration = 1000 end
    if (params.start == nil) then params.start = 0 end
    if (params.target == nil) then params.target = 0 end
    if (params.easing == nil) then params.easing = 0 end
    if (params.callback == nil) then params.callback = function()end end
    if (params.finalCallback == nil) then params.finalCallback = function()end end
    if (params.isCyclic == nil) then params.isCyclic = false end

    local anim = {}
    anim.id = params.id
    anim.duration = params.duration
    anim.start = params.start
    anim.target = params.target
    anim.diff = params.target - params.start
    anim.value = anim.start
    anim.time = 0
    anim.callback = params.callback
    anim.finalCallback = params.finalCallback
    anim.isCyclic = params.isCyclic
    anim.isCompleted = false

    if not (anim.id == nil) and (anim.id < self.animationsCount) then
        for i = 1, self.animationsCount - 1, 1 do
            if (self.animations[i].id == anim.id) then
                self.animations[i].isCompleted = true
            end
        end
    end

    self.animations[self.animationsCount] = anim

    return anim.id
end

function animator:release(id)
    for i = 1, self.animationsCount, 1 do
        if (self.animations[i].id == id) then
            self.animations[i].isCompleted = true
        end
    end
end

function animator:releaseAnimator()
    Runtime:removeEventListener("enterFrame", update)
    self.animations = {}
    self.animationsCount = 0
    self.isInitialized = false
end